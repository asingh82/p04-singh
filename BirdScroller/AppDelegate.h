//
//  AppDelegate.h
//  BirdScroller
//
//  Created by Anshima on 17/03/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

