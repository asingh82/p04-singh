//
//  ViewController.m
//  BirdScroller
//
//  Created by Anshima on 17/03/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

int intWidth;
int intHeight;
int birdUp;
int RandomTopPipePosition;
int RandomBottomPipePosition;
int scnWidth;
int scnHeight;

@implementation ViewController

@synthesize PipeBottom, PipeTop;
@synthesize start;
@synthesize bird;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self getScreenWidth];
}
-(void)getScreenWidth{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    intWidth = (int) roundf(screenWidth);
    intHeight = (int) roundf(screenHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)start:(id)sender{
    start.hidden=YES;
    PipeTop.hidden=NO;
    PipeBottom.hidden=NO;
    bird.center=CGPointMake(bird.center.x, 250);
    bird.hidden=NO;
    //For moving bird, calling birdMoving function
    MoveBird=[NSTimer scheduledTimerWithTimeInterval:0.07 target:self selector:@selector(birdMoving) userInfo:nil repeats:YES];
    [self columnMove];
     //For moving column, calling colMoving function
    MovePipe=[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(colMoving) userInfo:nil repeats:YES];
   
}
-(void)columnMove{
    int num =intHeight/2;
    RandomTopPipePosition=arc4random() % num;
    RandomTopPipePosition=RandomTopPipePosition-200;
    
    RandomBottomPipePosition=RandomTopPipePosition + 650;
    
    PipeTop.center=CGPointMake(intWidth+25, RandomTopPipePosition);
    PipeBottom.center=CGPointMake(intWidth+25, RandomBottomPipePosition);
    
}

-(void)colMoving{
    
    PipeTop.center=CGPointMake(PipeTop.center.x-1, PipeTop.center.y);
    PipeBottom.center=CGPointMake(PipeBottom.center.x-1, PipeBottom.center.y);
    
    
    if(PipeBottom.center.x<-60){
        [self columnMove];
    }
    if(CGRectIntersectsRect(bird.frame, PipeTop.frame)){
        NSLog(@"1 bird x Value: %f",bird.frame.origin.x);
        NSLog(@"bird y Value: %f",bird.frame.origin.y);
        NSLog(@"PipeTop x Value: %f",PipeTop.frame.origin.x);
        NSLog(@"PipeTop y Value: %f",PipeTop.frame.origin.y);
        NSLog(@"Pipebottom x Value: %f",PipeBottom.frame.origin.x);
        NSLog(@"Pipebottom y Value: %f",PipeBottom.frame.origin.y);
        [self gameOver];
    }

    if(CGRectIntersectsRect(bird.frame, PipeBottom.frame)){
        NSLog(@"2 bird x Value: %f",bird.frame.origin.x);
        NSLog(@"bird y Value: %f",bird.frame.origin.y);
        NSLog(@"PipeTop x Value: %f",PipeTop.frame.origin.x);
        NSLog(@"PipeTop y Value: %f",PipeTop.frame.origin.y);
        NSLog(@"Pipebottom x Value: %f",PipeBottom.frame.origin.x);
        NSLog(@"Pipebottom y Value: %f",PipeBottom.frame.origin.y);
        [self gameOver];
 
    }
    /*
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    scnWidth = (int) roundf(screenWidth);
    scnHeight = (int) roundf(screenHeight);
    if(scnHeight > bird.frame.size.height)
    {
        [self gameOver];
    }
    
    if(scnHeight < 0){
        [self gameOver];
    }
    */
    
    //code for game over
}
-(void)gameOver{
    
    [MovePipe invalidate];
    [MoveBird invalidate];
    PipeTop.hidden=YES;
    PipeBottom.hidden=YES;
    start.hidden=NO;
    bird.hidden=YES;
}

-(void) birdMoving{
    bird.center=CGPointMake(bird.center.x , bird.center.y+birdUp);
    birdUp=birdUp+5;
    
    if(birdUp>15){
        birdUp=15;
    }
    
    if(birdUp>0){
        bird.image=[UIImage imageNamed:@"Bird-blue.png"];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    birdUp=-30;
}

@end
