//
//  ViewController.h
//  BirdScroller
//
//  Created by Anshima on 17/03/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSTimer * MoveBird;
    NSTimer * MovePipe;
}

@property (weak,nonatomic)IBOutlet UIImageView * PipeTop;

@property (weak,nonatomic) IBOutlet UIImageView * PipeBottom;

@property (weak,nonatomic) IBOutlet UIImageView * bird;

@property (weak,nonatomic) IBOutlet UIButton * start;

- (IBAction)start:(id)sender;


@end

